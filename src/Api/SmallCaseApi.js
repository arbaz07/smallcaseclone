import axios from 'axios';

export function getSmallCase(scid){
  return axios.get(`/smallcases/smallcase?scid=${scid}`).
  then(res => res.data);
}

// getSmallCase("SCAW_0001").then(console.log)
export function getGraph(type,scid){
  return axios.get(`/smallcases/chart?scid=${scid}&duration=max&granularity=1m&endDate=2023-02-03&customAmount=100&benchmarkIndex=.NIFTY100&benchmarkType=index&type=${type}`)
    .then(res=>res.data);
}

// &duration=6m&benchmarkId=.NIFTY100&benchmarkType=index


const options = {
  method: 'GET',
  headers: {
    accept: 'application/json',
    'x-gateway-secret': 'gatewayDemo_secret',
    'x-gateway-authtoken': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJndWVzdCI6dHJ1ZSwiaWF0IjoxNjU0MzIxMDAwfQ.qiZ_w1yFYXhkdLMlqI28XJOXitfZwr64e2oL-lMEHZU'
  }
};

export function getChartData(duration){

  return fetch(`https://gatewayapi.smallcase.com/v1/gatewayDemo/engine/smallcase/chart?scid=SCET_0010&benchmarkId=.NIFTY100&duration=${duration}&base=100`, options)
    .then(response => response.json())
    .then(response => {
      return response
    })
    .catch(err => console.error(err));

}
