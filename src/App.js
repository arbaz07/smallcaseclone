import "./App.css";
import {ChakraProvider } from "@chakra-ui/react";
import Navbar from "./Components/Nav-bar/Navbar";
import Body from "./Components/Body";
import { Provider } from "react-redux";
import Footer from "./Components/Footer/Footer";

function App() {
  return (
    <ChakraProvider>
      <Navbar />

      <Body/>
      <Footer/>
    </ChakraProvider>
  );
}

export default App;
