import { ActionTypes } from "../constants/ActionTypes"

export const setData=(data)=>{
  return {
    type:ActionTypes.SET_DATA,
    payload:data,
  }
}
