import { ActionTypes } from "../constants/ActionTypes";


export const smallCaseReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_DATA:
      // console.log('payload :>> ', payload);
      return {
        data: [...payload],
      };

    default:
      return state;
  }
};

