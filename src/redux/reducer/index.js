import { combineReducers } from "redux";
import {smallCaseReducer} from './SmallCaseReducer'

const reducers = combineReducers(
  {
    data:smallCaseReducer,
  }
)
export default reducers;
