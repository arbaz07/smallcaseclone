
import React, { Component } from "react";
import { Navbar, Nav, Container, NavDropdown, Form } from "react-bootstrap";


export default class NavBar extends Component {
  state = {};
  render() {
    return (
      <Navbar>
        <Container style={{ borderBottom: "1px solid #bfbfbf" }}>
          <Nav style={{ gap: "30px" }}>
            <Nav.Link>Collections</Nav.Link>
            <Nav.Link style={{ borderBottom: "2px solid blue", color: "blue" }}>
              All smallcases
            </Nav.Link>
            <Nav.Link>Managers</Nav.Link>
          </Nav>
          <Nav style={{ gap: "40px" }}>
            <NavDropdown
              title="Sort by Popularity"
              id="collasible-nav-dropdown"
              style={{ borderBottom: "2px solid black" }}
            >
              <Form.Check className="mb-2" type="radio" label="Popularity" />
              <Form.Check
                className="mb-2"
                type="radio"
                label="Minimum Amount"
              />
            </NavDropdown>
          </Nav>
        </Container>
      </Navbar>
    );
  }
}
