import { Box, Text, Flex, Button } from "@chakra-ui/react";
import React, { Component } from "react";
// import Table

export default class ChartHeading extends Component {
  render() {
    return (
      <div >
        <Flex margin='10'justify="space-between" fontSize={20}>
          <Box>
            <Text>Past Performance vs Equity Large Cap</Text>
          </Box>
          <Flex fontSize='20'>
            <Button backgroundColor='white' onClick={()=>this.props.onChartChange('1M')}>1M</Button>
            <Button backgroundColor='white' onClick={()=>this.props.onChartChange('6M')}>6M</Button>
            <Button backgroundColor='white' onClick={()=>this.props.onChartChange('1Y')}>1Y</Button>
            <Button backgroundColor='white' onClick={()=>this.props.onChartChange('3Y')}>3Y</Button>
            <Button backgroundColor='#e8f1fc' onClick={()=>this.props.onChartChange('Max')}>Max</Button>
            <Button  backgroundColor='white' onClick={()=>this.props.onChartChange('SIP')}>SIP</Button>
            
          </Flex>
          
        </Flex>
      </div>
    );
  }
}
