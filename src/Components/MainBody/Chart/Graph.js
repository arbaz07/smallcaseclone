import React, { Component } from "react";
import { CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis } from "recharts";

import { getGraph } from "../../../Api/SmallCaseApi";
import ChartHeading from "./ChartHeading";

export default class Graph extends Component {
  constructor(props) {
    super(props);

    this.state = {
      graph: "",
      err: false,
    };
  }

  componentDidMount = async () => {
    try {
      const response = await getGraph("SIP", this.props.scid);
      this.setState({
        graph: response.data,
      });
    } catch (err) {
      this.setState({
        err: true,
      });
    }
  };
  render() {
    if (this.state.graph === "") {
      return <div>Loading.........</div>;
    }
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
    const { userSipHistorical, niftySipHistorical, smallcaseSipHistorical,userInvestmentAmount,smallcaseAmount,niftyAmount } = this.state.graph;
    const dates = userSipHistorical.map((item) => {
      let newDate = new Date(item.date)
      return months[newDate.getMonth()]+" "+newDate.getFullYear();
    });

    const userSipAmount = userSipHistorical.map(item=>item.amount);
    const niftySipAmount = niftySipHistorical.map(item=>item.amount);
    const smallcaseSipAmount = smallcaseSipHistorical.map(item=>item.amount);
    let dataObj=[];
    for (let index = 0; index<dates.length; index++){
      dataObj.push({name:dates[index], TotalInvestment:userSipAmount[index],EquityLargeCap:niftySipAmount[index],EquityAndDebt:smallcaseSipAmount[index]})
    }
    // console.log(" :>> ", dataObj);
    return (
      <div>
        <ChartHeading 
          inv ={userInvestmentAmount}
          elc = {niftyAmount}
          eac = {smallcaseAmount}
        />
        <LineChart width={1200} height={400} data={dataObj}>
          <Line  type="monotone" dataKey="TotalInvestment" stroke="green" strokeWidth={3}/>
          <Line  type="monotone" dataKey="EquityLargeCap" stroke="#e3af64"  strokeWidth={3}/>
          <Line  type="monotone" dataKey="EquityAndDebt" stroke="#1f7ae0"  strokeWidth={3}/>
          {/* <CartesianGrid stroke="#ccc"/> */}
          <XAxis dataKey={"name"}/>
          <YAxis/>
          <Tooltip />
        </LineChart>
       
      </div>
    );
  }
}
