import { Box, Text, Flex } from "@chakra-ui/react";
import React, { Component } from "react";
// import Table

export default class ChartHeading extends Component {
  render() {
    const {elc,inv,eac} =this.props
    return (
      <div>
        <Flex justify="space-around" fontSize={20}>
          <Box>
            <Text>
              Current value of ₹ 100 invested monthly since launch would be
            </Text>
          </Box>
          <Box>
            <Text color="#1f7ae0">Equity & Debt</Text>
            <Text> ₹ {eac.toFixed(2)}</Text>
          </Box>
          <Box>
            <Text color="#e3af64">Equity Large Cap</Text>
            <Text> ₹ {elc.toFixed(2)}</Text>
          </Box>
          <Box>
            <Text color="#19af55">Total Investment</Text>
            <Text> ₹ {inv}</Text>
          </Box>
        </Flex>
      </div>
    );
  }
}
