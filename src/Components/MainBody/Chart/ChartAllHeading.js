import { Box, Text, Flex } from "@chakra-ui/react";
import React, { Component } from "react";
// import Table

export default class ChartAllHeading extends Component {
  render() {
    const {elc,eac} =this.props
    return (
      <div>
        <Flex justify="space-around" fontSize={20}>
          <Box>
            <Text>
              Current value of ₹ 100 invested monthly since launch would be
            </Text>
          </Box>
          <Box>
            <Text color="#1f7ae0">Equity & Debt</Text>
            <Text> ₹ {eac}</Text>
          </Box>
          <Box>
            <Text color="#e3af64">Equity Large Cap</Text>
            <Text> ₹ {elc}</Text>
          </Box>
        </Flex>
      </div>
    );
  }
}
