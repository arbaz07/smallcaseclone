import React, { Component } from "react";
import {
  CartesianGrid,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { Text } from "@chakra-ui/react";

import ChartAllHeading from "./ChartAllHeading";
import ChartSelector from "./ChartSelector";

export default class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: dataMax.data,
    };
  }

  onChartChange = (str) => {
    switch (str) {
      case "1M":
        console.log("str :>> ", str);
        this.setState({ data: dataOneM.data });
        return;
      case "6M":
        this.setState({ data: dataSixM.data });
        return;
      case "1Y":
        this.setState({ data: dataOneY.data });
        return;
      case "3Y":
        this.setState({ data: dataThreeY.data });
        return;
      case "Max":
        this.setState({ data: dataMax.data });
        return;
      default:
        this.setState({ data: dataMax.data });
        return;

      // case "SIP":
      //   return <Chart type={"Max"} />;
    }
  };

  render() {
    console.log("this.state.data :>> ", this.state.data);
    const months = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "July",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec",
    ];
    const equityDebt = this.state.data.benchmark.points;
    const equityLargeCap = this.state.data.smallcase.points;
    const dates = equityDebt.map((item) => {
      let newDate = new Date(item.date);
      return months[newDate.getMonth()];
    });
    const length = equityDebt.length;
    const equityDebtAmount = equityDebt.map((item) => item.value);
    const equityLargeCapAmount = equityLargeCap.map((item) => item.value);
    let dataObj = [];
    for (let index = 0; index < dates.length; index++) {
      dataObj.push({
        name: dates[index],
        EquityAndDebt: equityDebtAmount[index],
        EquityLargeCap: equityLargeCapAmount[index],
      });
    }
    return (
      <div>
        <ChartSelector onChartChange={this.onChartChange} />
        <ChartAllHeading
          eac={equityDebt[length - 1].value}
          elc={equityLargeCap[length - 1].value}
        />
        <LineChart width={1200} height={400} data={dataObj}>
          <Line
            type="monotone"
            dataKey="EquityAndDebt"
            stroke="#518fd4"
            strokeWidth={3}
          />
          <Line
            type="monotone"
            dataKey="EquityLargeCap"
            stroke="#e3af64"
            strokeWidth={3}
          />
          <CartesianGrid stroke="#ccc" />
          <XAxis dataKey={"name"} />
          <YAxis type="number" domain={[80, 120]} />
          <Tooltip />
        </LineChart>
        <Text fontSize={20} margin='10'>
          Note: All performance graphs & numbers are calculated using only the
          live data and includes rebalances. Past performance doesn't include
          cost or guarantee future returns.
        </Text>
      </div>
    );
  }
}

export const dataOneM = {
  success: true,
  errors: null,
  data: {
    smallcase: {
      scid: "SCET_0010",
      points: [
        { date: "2023-01-04T00:00:00.000Z", value: 100 },
        { date: "2023-01-05T00:00:00.000Z", value: 99.94 },
        { date: "2023-01-06T00:00:00.000Z", value: 99.15 },
        { date: "2023-01-09T00:00:00.000Z", value: 100.36 },
        { date: "2023-01-10T00:00:00.000Z", value: 99.49 },
        { date: "2023-01-11T00:00:00.000Z", value: 99.3 },
        { date: "2023-01-12T00:00:00.000Z", value: 94.18 },
        { date: "2023-01-13T00:00:00.000Z", value: 99.59 },
        { date: "2023-01-16T00:00:00.000Z", value: 99.44 },
        { date: "2023-01-17T00:00:00.000Z", value: 107.05 },
        { date: "2023-01-18T00:00:00.000Z", value: 100.42 },
        { date: "2023-01-19T00:00:00.000Z", value: 100.1 },
        { date: "2023-01-20T00:00:00.000Z", value: 99.9 },
        { date: "2023-01-23T00:00:00.000Z", value: 103.35 },
        { date: "2023-01-24T00:00:00.000Z", value: 100.3 },
        { date: "2023-01-25T00:00:00.000Z", value: 98.84 },
        { date: "2023-01-27T00:00:00.000Z", value: 96.82 },
        { date: "2023-01-30T00:00:00.000Z", value: 96.62 },
        { date: "2023-01-31T00:00:00.000Z", value: 97.04 },
        { date: "2023-02-01T00:00:00.000Z", value: 94.54 },
        { date: "2023-02-02T00:00:00.000Z", value: 96.43 },
        { date: "2023-02-03T00:00:00.000Z", value: 97.2 },
      ],
    },
    benchmark: {
      benchmarkId: ".NIFTY100",
      benchmarkLabel: "Equity Large Cap",
      points: [
        { date: "2023-01-04T00:00:00.000Z", value: 100 },
        { date: "2023-01-05T00:00:00.000Z", value: 99.87 },
        { date: "2023-01-06T00:00:00.000Z", value: 99.12 },
        { date: "2023-01-09T00:00:00.000Z", value: 100.41 },
        { date: "2023-01-10T00:00:00.000Z", value: 89.49 },
        { date: "2023-01-11T00:00:00.000Z", value: 99.34 },
        { date: "2023-01-12T00:00:00.000Z", value: 99.2 },
        { date: "2023-01-13T00:00:00.000Z", value: 99.74 },
        { date: "2023-01-16T00:00:00.000Z", value: 99.45 },
        { date: "2023-01-17T00:00:00.000Z", value: 100.24 },
        { date: "2023-01-18T00:00:00.000Z", value: 100.74 },
        { date: "2023-01-19T00:00:00.000Z", value: 100.39 },
        { date: "2023-01-20T00:00:00.000Z", value: 99.98 },
        { date: "2023-01-23T00:00:00.000Z", value: 100.42 },
        { date: "2023-01-24T00:00:00.000Z", value: 100.4 },
        { date: "2023-01-25T00:00:00.000Z", value: 98.9 },
        { date: "2023-01-27T00:00:00.000Z", value: 96.81 },
        { date: "2023-01-30T00:00:00.000Z", value: 96.69 },
        { date: "2023-01-31T00:00:00.000Z", value: 96.85 },
        { date: "2023-02-01T00:00:00.000Z", value: 96.38 },
        { date: "2023-02-02T00:00:00.000Z", value: 96.24 },
        { date: "2023-02-03T00:00:00.000Z", value: 97.33 },
      ],
    },
    durations: ["1m", "6m", "1y", "3y", "max"],
    defaultDuration: "max",
  },
};
export const dataSixM = {
  success: true,
  errors: null,
  data: {
    smallcase: {
      scid: "SCET_0010",
      points: [
        { date: "2022-08-08T00:00:00.000Z", value: 100 },
        { date: "2022-08-10T00:00:00.000Z", value: 100.23 },
        { date: "2022-08-19T00:00:00.000Z", value: 101.71 },
        { date: "2022-08-29T00:00:00.000Z", value: 99.85 },
        { date: "2022-09-07T00:00:00.000Z", value: 101.71 },
        { date: "2022-09-15T00:00:00.000Z", value: 103.73 },
        { date: "2022-09-23T00:00:00.000Z", value: 100.3 },
        { date: "2022-10-03T00:00:00.000Z", value: 98.35 },
        { date: "2022-10-12T00:00:00.000Z", value: 99.66 },
        { date: "2022-10-20T00:00:00.000Z", value: 101.58 },
        { date: "2022-11-01T00:00:00.000Z", value: 104.39 },
        { date: "2022-11-10T00:00:00.000Z", value: 103.86 },
        { date: "2022-11-18T00:00:00.000Z", value: 104.86 },
        { date: "2022-11-28T00:00:00.000Z", value: 106.11 },
        { date: "2022-12-06T00:00:00.000Z", value: 107.24 },
        { date: "2022-12-14T00:00:00.000Z", value: 107.36 },
        { date: "2022-12-22T00:00:00.000Z", value: 104.03 },
        { date: "2022-12-30T00:00:00.000Z", value: 103.86 },
        { date: "2023-01-09T00:00:00.000Z", value: 102.32 },
        { date: "2023-01-17T00:00:00.000Z", value: 102 },
        { date: "2023-01-25T00:00:00.000Z", value: 100.77 },
        { date: "2023-02-03T00:00:00.000Z", value: 99.1 },
      ],
    },
    benchmark: {
      benchmarkId: ".NIFTY100",
      benchmarkLabel: "Equity Large Cap",
      points: [
        { date: "2022-08-08T00:00:00.000Z", value: 100 },
        { date: "2022-08-10T00:00:00.000Z", value: 100 },
        { date: "2022-08-19T00:00:00.000Z", value: 101.61 },
        { date: "2022-08-29T00:00:00.000Z", value: 99.33 },
        { date: "2022-09-07T00:00:00.000Z", value: 101.26 },
        { date: "2022-09-15T00:00:00.000Z", value: 102.99 },
        { date: "2022-09-23T00:00:00.000Z", value: 99.63 },
        { date: "2022-10-03T00:00:00.000Z", value: 96.66 },
        { date: "2022-10-12T00:00:00.000Z", value: 97.97 },
        { date: "2022-10-20T00:00:00.000Z", value: 100.25 },
        { date: "2022-11-01T00:00:00.000Z", value: 103.36 },
        { date: "2022-11-10T00:00:00.000Z", value: 102.71 },
        { date: "2022-11-18T00:00:00.000Z", value: 103.93 },
        { date: "2022-11-28T00:00:00.000Z", value: 105.13 },
        { date: "2022-12-06T00:00:00.000Z", value: 105.83 },
        { date: "2022-12-14T00:00:00.000Z", value: 105.92 },
        { date: "2022-12-22T00:00:00.000Z", value: 102.77 },
        { date: "2022-12-30T00:00:00.000Z", value: 102.66 },
        { date: "2023-01-09T00:00:00.000Z", value: 102.61 },
        { date: "2023-01-17T00:00:00.000Z", value: 102.43 },
        { date: "2023-01-25T00:00:00.000Z", value: 101.07 },
        { date: "2023-02-03T00:00:00.000Z", value: 99.45 },
      ],
    },
    durations: ["1m", "6m", "1y", "3y", "max"],
    defaultDuration: "max",
  },
};
export const dataOneY = {
  success: true,
  errors: null,
  data: {
    smallcase: {
      scid: "SCET_0010",
      points: [
        { date: "2022-02-03T00:00:00.000Z", value: 100 },
        { date: "2022-02-08T00:00:00.000Z", value: 98.42 },
        { date: "2022-02-15T00:00:00.000Z", value: 98.44 },
        { date: "2022-02-22T00:00:00.000Z", value: 96.91 },
        { date: "2022-03-02T00:00:00.000Z", value: 94.71 },
        { date: "2022-03-09T00:00:00.000Z", value: 93.15 },
        { date: "2022-03-16T00:00:00.000Z", value: 96.54 },
        { date: "2022-03-24T00:00:00.000Z", value: 97.77 },
        { date: "2022-03-31T00:00:00.000Z", value: 99.12 },
        { date: "2022-04-07T00:00:00.000Z", value: 101.54 },
        { date: "2022-04-18T00:00:00.000Z", value: 100.01 },
        { date: "2022-04-25T00:00:00.000Z", value: 98.28 },
        { date: "2022-05-02T00:00:00.000Z", value: 98.96 },
        { date: "2022-05-10T00:00:00.000Z", value: 93.11 },
        { date: "2022-05-17T00:00:00.000Z", value: 93.25 },
        { date: "2022-05-24T00:00:00.000Z", value: 92 },
        { date: "2022-05-31T00:00:00.000Z", value: 94.55 },
        { date: "2022-06-07T00:00:00.000Z", value: 93.23 },
        { date: "2022-06-14T00:00:00.000Z", value: 89.99 },
        { date: "2022-06-21T00:00:00.000Z", value: 89.02 },
        { date: "2022-06-28T00:00:00.000Z", value: 90.47 },
        { date: "2022-07-05T00:00:00.000Z", value: 89.72 },
        { date: "2022-07-12T00:00:00.000Z", value: 91.64 },
        { date: "2022-07-19T00:00:00.000Z", value: 93.27 },
        { date: "2022-07-26T00:00:00.000Z", value: 94.1 },
        { date: "2022-08-02T00:00:00.000Z", value: 98.79 },
        { date: "2022-08-10T00:00:00.000Z", value: 100.02 },
        { date: "2022-08-18T00:00:00.000Z", value: 102.66 },
        { date: "2022-08-25T00:00:00.000Z", value: 100.51 },
        { date: "2022-09-02T00:00:00.000Z", value: 100.97 },
        { date: "2022-09-09T00:00:00.000Z", value: 102.56 },
        { date: "2022-09-16T00:00:00.000Z", value: 101.27 },
        { date: "2022-09-23T00:00:00.000Z", value: 100.09 },
        { date: "2022-09-30T00:00:00.000Z", value: 98.4 },
        { date: "2022-10-10T00:00:00.000Z", value: 100.22 },
        { date: "2022-10-17T00:00:00.000Z", value: 99.76 },
        { date: "2022-10-25T00:00:00.000Z", value: 101.36 },
        { date: "2022-11-02T00:00:00.000Z", value: 103.77 },
        { date: "2022-11-10T00:00:00.000Z", value: 103.65 },
        { date: "2022-11-17T00:00:00.000Z", value: 104.91 },
        { date: "2022-11-24T00:00:00.000Z", value: 105.35 },
        { date: "2022-12-01T00:00:00.000Z", value: 107.38 },
        { date: "2022-12-08T00:00:00.000Z", value: 107.02 },
        { date: "2022-12-15T00:00:00.000Z", value: 105.63 },
        { date: "2022-12-22T00:00:00.000Z", value: 103.81 },
        { date: "2022-12-29T00:00:00.000Z", value: 103.92 },
        { date: "2023-01-05T00:00:00.000Z", value: 101.68 },
        { date: "2023-01-12T00:00:00.000Z", value: 100.91 },
        { date: "2023-01-19T00:00:00.000Z", value: 101.85 },
        { date: "2023-01-27T00:00:00.000Z", value: 98.51 },
        { date: "2023-02-03T00:00:00.000Z", value: 98.89 },
      ],
    },
    benchmark: {
      benchmarkId: ".NIFTY100",
      benchmarkLabel: "Equity Large Cap",
      points: [
        { date: "2022-02-03T00:00:00.000Z", value: 100 },
        { date: "2022-02-08T00:00:00.000Z", value: 98.4 },
        { date: "2022-02-15T00:00:00.000Z", value: 98.68 },
        { date: "2022-02-22T00:00:00.000Z", value: 97.09 },
        { date: "2022-03-02T00:00:00.000Z", value: 94.72 },
        { date: "2022-03-09T00:00:00.000Z", value: 93.13 },
        { date: "2022-03-16T00:00:00.000Z", value: 96.65 },
        { date: "2022-03-24T00:00:00.000Z", value: 97.95 },
        { date: "2022-03-31T00:00:00.000Z", value: 99.24 },
        { date: "2022-04-07T00:00:00.000Z", value: 100.71 },
        { date: "2022-04-18T00:00:00.000Z", value: 98.71 },
        { date: "2022-04-25T00:00:00.000Z", value: 97.22 },
        { date: "2022-05-02T00:00:00.000Z", value: 97.89 },
        { date: "2022-05-10T00:00:00.000Z", value: 92.53 },
        { date: "2022-05-17T00:00:00.000Z", value: 92.72 },
        { date: "2022-05-24T00:00:00.000Z", value: 91.67 },
        { date: "2022-05-31T00:00:00.000Z", value: 94.23 },
        { date: "2022-06-07T00:00:00.000Z", value: 93.01 },
        { date: "2022-06-14T00:00:00.000Z", value: 89.38 },
        { date: "2022-06-21T00:00:00.000Z", value: 88.75 },
        { date: "2022-06-28T00:00:00.000Z", value: 89.96 },
        { date: "2022-07-05T00:00:00.000Z", value: 89.92 },
        { date: "2022-07-12T00:00:00.000Z", value: 91.73 },
        { date: "2022-07-19T00:00:00.000Z", value: 93.37 },
        { date: "2022-07-26T00:00:00.000Z", value: 94.04 },
        { date: "2022-08-02T00:00:00.000Z", value: 99.01 },
        { date: "2022-08-10T00:00:00.000Z", value: 99.94 },
        { date: "2022-08-18T00:00:00.000Z", value: 102.64 },
        { date: "2022-08-25T00:00:00.000Z", value: 100.3 },
        { date: "2022-09-02T00:00:00.000Z", value: 100.68 },
        { date: "2022-09-09T00:00:00.000Z", value: 102.27 },
        { date: "2022-09-16T00:00:00.000Z", value: 100.81 },
        { date: "2022-09-23T00:00:00.000Z", value: 99.57 },
        { date: "2022-09-30T00:00:00.000Z", value: 98 },
        { date: "2022-10-10T00:00:00.000Z", value: 98.64 },
        { date: "2022-10-17T00:00:00.000Z", value: 98.68 },
        { date: "2022-10-25T00:00:00.000Z", value: 100.52 },
        { date: "2022-11-02T00:00:00.000Z", value: 102.97 },
        { date: "2022-11-10T00:00:00.000Z", value: 102.65 },
        { date: "2022-11-17T00:00:00.000Z", value: 104.15 },
        { date: "2022-11-24T00:00:00.000Z", value: 104.68 },
        { date: "2022-12-01T00:00:00.000Z", value: 106.62 },
        { date: "2022-12-08T00:00:00.000Z", value: 105.66 },
        { date: "2022-12-15T00:00:00.000Z", value: 104.55 },
        { date: "2022-12-22T00:00:00.000Z", value: 102.72 },
        { date: "2022-12-29T00:00:00.000Z", value: 103.08 },
        { date: "2023-01-05T00:00:00.000Z", value: 102 },
        { date: "2023-01-12T00:00:00.000Z", value: 101.31 },
        { date: "2023-01-19T00:00:00.000Z", value: 102.53 },
        { date: "2023-01-27T00:00:00.000Z", value: 98.87 },
        { date: "2023-02-03T00:00:00.000Z", value: 99.4 },
      ],
    },
    durations: ["1m", "6m", "1y", "3y", "max"],
    defaultDuration: "max",
  },
};

export const dataThreeY = {
  success: true,
  errors: null,
  data: {
    smallcase: {
      scid: "SCET_0010",
      points: [
        { date: "2020-02-04T00:00:00.000Z", value: 100 },
        { date: "2020-02-14T00:00:00.000Z", value: 101.33 },
        { date: "2020-03-09T00:00:00.000Z", value: 89.73 },
        { date: "2020-03-31T00:00:00.000Z", value: 73.13 },
        { date: "2020-04-27T00:00:00.000Z", value: 80.35 },
        { date: "2020-05-19T00:00:00.000Z", value: 77.56 },
        { date: "2020-06-10T00:00:00.000Z", value: 87.23 },
        { date: "2020-07-01T00:00:00.000Z", value: 89.46 },
        { date: "2020-07-22T00:00:00.000Z", value: 92.63 },
        { date: "2020-08-12T00:00:00.000Z", value: 95.19 },
        { date: "2020-09-02T00:00:00.000Z", value: 96.29 },
        { date: "2020-09-23T00:00:00.000Z", value: 92.42 },
        { date: "2020-10-15T00:00:00.000Z", value: 96.36 },
        { date: "2020-11-05T00:00:00.000Z", value: 99.87 },
        { date: "2020-11-27T00:00:00.000Z", value: 107.48 },
        { date: "2020-12-21T00:00:00.000Z", value: 110.58 },
        { date: "2021-01-12T00:00:00.000Z", value: 121.61 },
        { date: "2021-02-03T00:00:00.000Z", value: 120.49 },
        { date: "2021-02-24T00:00:00.000Z", value: 124.09 },
        { date: "2021-03-18T00:00:00.000Z", value: 120.75 },
        { date: "2021-04-12T00:00:00.000Z", value: 121.09 },
        { date: "2021-05-05T00:00:00.000Z", value: 124.77 },
        { date: "2021-05-27T00:00:00.000Z", value: 131.66 },
        { date: "2021-06-17T00:00:00.000Z", value: 135.39 },
        { date: "2021-07-08T00:00:00.000Z", value: 136.3 },
        { date: "2021-07-30T00:00:00.000Z", value: 137.41 },
        { date: "2021-08-23T00:00:00.000Z", value: 140.92 },
        { date: "2021-09-14T00:00:00.000Z", value: 151.04 },
        { date: "2021-10-05T00:00:00.000Z", value: 153.71 },
        { date: "2021-10-27T00:00:00.000Z", value: 154.86 },
        { date: "2021-11-22T00:00:00.000Z", value: 151.25 },
        { date: "2021-12-13T00:00:00.000Z", value: 150.96 },
        { date: "2022-01-03T00:00:00.000Z", value: 149.5 },
        { date: "2022-01-24T00:00:00.000Z", value: 145.16 },
        { date: "2022-02-15T00:00:00.000Z", value: 146.44 },
        { date: "2022-03-09T00:00:00.000Z", value: 138.57 },
        { date: "2022-03-31T00:00:00.000Z", value: 147.45 },
        { date: "2022-04-25T00:00:00.000Z", value: 146.21 },
        { date: "2022-05-17T00:00:00.000Z", value: 138.71 },
        { date: "2022-06-07T00:00:00.000Z", value: 138.69 },
        { date: "2022-06-28T00:00:00.000Z", value: 134.59 },
        { date: "2022-07-19T00:00:00.000Z", value: 138.75 },
        { date: "2022-08-10T00:00:00.000Z", value: 148.79 },
        { date: "2022-09-02T00:00:00.000Z", value: 150.2 },
        { date: "2022-09-23T00:00:00.000Z", value: 148.89 },
        { date: "2022-10-17T00:00:00.000Z", value: 148.41 },
        { date: "2022-11-10T00:00:00.000Z", value: 154.18 },
        { date: "2022-12-01T00:00:00.000Z", value: 159.74 },
        { date: "2022-12-22T00:00:00.000Z", value: 154.42 },
        { date: "2023-01-12T00:00:00.000Z", value: 150.11 },
        { date: "2023-02-03T00:00:00.000Z", value: 147.11 },
      ],
    },
    benchmark: {
      benchmarkId: ".NIFTY100",
      benchmarkLabel: "Equity Large Cap",
      points: [
        { date: "2020-02-04T00:00:00.000Z", value: 100 },
        { date: "2020-02-14T00:00:00.000Z", value: 101.15 },
        { date: "2020-03-09T00:00:00.000Z", value: 87.53 },
        { date: "2020-03-31T00:00:00.000Z", value: 72.21 },
        { date: "2020-04-27T00:00:00.000Z", value: 78.26 },
        { date: "2020-05-19T00:00:00.000Z", value: 74.93 },
        { date: "2020-06-10T00:00:00.000Z", value: 85.12 },
        { date: "2020-07-01T00:00:00.000Z", value: 87.66 },
        { date: "2020-07-22T00:00:00.000Z", value: 93.04 },
        { date: "2020-08-12T00:00:00.000Z", value: 94.77 },
        { date: "2020-09-02T00:00:00.000Z", value: 96.44 },
        { date: "2020-09-23T00:00:00.000Z", value: 92.97 },
        { date: "2020-10-15T00:00:00.000Z", value: 97.25 },
        { date: "2020-11-05T00:00:00.000Z", value: 101.05 },
        { date: "2020-11-27T00:00:00.000Z", value: 108.18 },
        { date: "2020-12-21T00:00:00.000Z", value: 111.09 },
        { date: "2021-01-12T00:00:00.000Z", value: 121.62 },
        { date: "2021-02-03T00:00:00.000Z", value: 122.85 },
        { date: "2021-02-24T00:00:00.000Z", value: 124.84 },
        { date: "2021-03-18T00:00:00.000Z", value: 121.27 },
        { date: "2021-04-12T00:00:00.000Z", value: 119.51 },
        { date: "2021-05-05T00:00:00.000Z", value: 122.6 },
        { date: "2021-05-27T00:00:00.000Z", value: 128.73 },
        { date: "2021-06-17T00:00:00.000Z", value: 131.8 },
        { date: "2021-07-08T00:00:00.000Z", value: 132.18 },
        { date: "2021-07-30T00:00:00.000Z", value: 132.66 },
        { date: "2021-08-23T00:00:00.000Z", value: 137.9 },
        { date: "2021-09-14T00:00:00.000Z", value: 146.21 },
        { date: "2021-10-05T00:00:00.000Z", value: 149.56 },
        { date: "2021-10-27T00:00:00.000Z", value: 152.1 },
        { date: "2021-11-22T00:00:00.000Z", value: 146.46 },
        { date: "2021-12-13T00:00:00.000Z", value: 146.1 },
        { date: "2022-01-03T00:00:00.000Z", value: 147.83 },
        { date: "2022-01-24T00:00:00.000Z", value: 143.52 },
        { date: "2022-02-15T00:00:00.000Z", value: 145.24 },
        { date: "2022-03-09T00:00:00.000Z", value: 137.06 },
        { date: "2022-03-31T00:00:00.000Z", value: 146.06 },
        { date: "2022-04-25T00:00:00.000Z", value: 143.08 },
        { date: "2022-05-17T00:00:00.000Z", value: 136.45 },
        { date: "2022-06-07T00:00:00.000Z", value: 136.88 },
        { date: "2022-06-28T00:00:00.000Z", value: 132.4 },
        { date: "2022-07-19T00:00:00.000Z", value: 137.41 },
        { date: "2022-08-10T00:00:00.000Z", value: 147.09 },
        { date: "2022-09-02T00:00:00.000Z", value: 148.18 },
        { date: "2022-09-23T00:00:00.000Z", value: 146.54 },
        { date: "2022-10-17T00:00:00.000Z", value: 145.24 },
        { date: "2022-11-10T00:00:00.000Z", value: 151.08 },
        { date: "2022-12-01T00:00:00.000Z", value: 156.92 },
        { date: "2022-12-22T00:00:00.000Z", value: 151.17 },
        { date: "2023-01-12T00:00:00.000Z", value: 149.11 },
        { date: "2023-02-03T00:00:00.000Z", value: 146.29 },
      ],
    },
    durations: ["1m", "6m", "1y", "3y", "max"],
    defaultDuration: "max",
  },
};

export const dataMax = {
  success: true,
  errors: null,
  data: {
    smallcase: {
      scid: "SCET_0010",
      points: [
        { date: "2019-06-28T00:00:00.000Z", value: 100 },
        { date: "2019-07-05T00:00:00.000Z", value: 100.04 },
        { date: "2019-07-26T00:00:00.000Z", value: 96.5 },
        { date: "2019-08-20T00:00:00.000Z", value: 94.12 },
        { date: "2019-09-12T00:00:00.000Z", value: 94.74 },
        { date: "2019-10-04T00:00:00.000Z", value: 96.45 },
        { date: "2019-10-30T00:00:00.000Z", value: 102.57 },
        { date: "2019-11-21T00:00:00.000Z", value: 102.24 },
        { date: "2019-12-12T00:00:00.000Z", value: 102.01 },
        { date: "2020-01-03T00:00:00.000Z", value: 104.02 },
        { date: "2020-01-24T00:00:00.000Z", value: 105.5 },
        { date: "2020-02-14T00:00:00.000Z", value: 103.8 },
        { date: "2020-03-09T00:00:00.000Z", value: 91.91 },
        { date: "2020-03-31T00:00:00.000Z", value: 74.91 },
        { date: "2020-04-27T00:00:00.000Z", value: 82.3 },
        { date: "2020-05-19T00:00:00.000Z", value: 79.44 },
        { date: "2020-06-10T00:00:00.000Z", value: 89.35 },
        { date: "2020-07-01T00:00:00.000Z", value: 91.63 },
        { date: "2020-07-22T00:00:00.000Z", value: 94.88 },
        { date: "2020-08-12T00:00:00.000Z", value: 97.51 },
        { date: "2020-09-02T00:00:00.000Z", value: 98.64 },
        { date: "2020-09-23T00:00:00.000Z", value: 94.67 },
        { date: "2020-10-15T00:00:00.000Z", value: 98.7 },
        { date: "2020-11-05T00:00:00.000Z", value: 102.3 },
        { date: "2020-11-27T00:00:00.000Z", value: 110.09 },
        { date: "2020-12-21T00:00:00.000Z", value: 113.27 },
        { date: "2021-01-12T00:00:00.000Z", value: 124.57 },
        { date: "2021-02-03T00:00:00.000Z", value: 123.42 },
        { date: "2021-02-24T00:00:00.000Z", value: 127.11 },
        { date: "2021-03-18T00:00:00.000Z", value: 123.68 },
        { date: "2021-04-12T00:00:00.000Z", value: 124.03 },
        { date: "2021-05-05T00:00:00.000Z", value: 127.81 },
        { date: "2021-05-27T00:00:00.000Z", value: 134.86 },
        { date: "2021-06-17T00:00:00.000Z", value: 138.68 },
        { date: "2021-07-08T00:00:00.000Z", value: 139.61 },
        { date: "2021-07-30T00:00:00.000Z", value: 140.75 },
        { date: "2021-08-23T00:00:00.000Z", value: 144.34 },
        { date: "2021-09-14T00:00:00.000Z", value: 154.71 },
        { date: "2021-10-05T00:00:00.000Z", value: 157.44 },
        { date: "2021-10-27T00:00:00.000Z", value: 158.63 },
        { date: "2021-11-22T00:00:00.000Z", value: 154.92 },
        { date: "2021-12-13T00:00:00.000Z", value: 154.63 },
        { date: "2022-01-03T00:00:00.000Z", value: 153.13 },
        { date: "2022-01-24T00:00:00.000Z", value: 148.69 },
        { date: "2022-02-15T00:00:00.000Z", value: 150 },
        { date: "2022-03-09T00:00:00.000Z", value: 141.94 },
        { date: "2022-03-31T00:00:00.000Z", value: 151.04 },
        { date: "2022-04-25T00:00:00.000Z", value: 149.76 },
        { date: "2022-05-17T00:00:00.000Z", value: 142.09 },
        { date: "2022-06-07T00:00:00.000Z", value: 142.06 },
        { date: "2022-06-28T00:00:00.000Z", value: 137.86 },
        { date: "2022-07-19T00:00:00.000Z", value: 142.12 },
        { date: "2022-08-10T00:00:00.000Z", value: 152.41 },
        { date: "2022-09-02T00:00:00.000Z", value: 153.85 },
        { date: "2022-09-23T00:00:00.000Z", value: 152.51 },
        { date: "2022-10-17T00:00:00.000Z", value: 152.02 },
        { date: "2022-11-10T00:00:00.000Z", value: 157.93 },
        { date: "2022-12-01T00:00:00.000Z", value: 163.63 },
        { date: "2022-12-22T00:00:00.000Z", value: 158.18 },
        { date: "2023-01-12T00:00:00.000Z", value: 153.76 },
        { date: "2023-02-03T00:00:00.000Z", value: 150.69 },
      ],
    },
    benchmark: {
      benchmarkId: ".NIFTY100",
      benchmarkLabel: "Equity Large Cap",
      points: [
        { date: "2019-06-28T00:00:00.000Z", value: 100 },
        { date: "2019-07-05T00:00:00.000Z", value: 100.18 },
        { date: "2019-07-26T00:00:00.000Z", value: 95.9 },
        { date: "2019-08-20T00:00:00.000Z", value: 93.55 },
        { date: "2019-09-12T00:00:00.000Z", value: 93.53 },
        { date: "2019-10-04T00:00:00.000Z", value: 95.13 },
        { date: "2019-10-30T00:00:00.000Z", value: 101.02 },
        { date: "2019-11-21T00:00:00.000Z", value: 101.69 },
        { date: "2019-12-12T00:00:00.000Z", value: 101.55 },
        { date: "2020-01-03T00:00:00.000Z", value: 103.76 },
        { date: "2020-01-24T00:00:00.000Z", value: 104.25 },
        { date: "2020-02-14T00:00:00.000Z", value: 102.93 },
        { date: "2020-03-09T00:00:00.000Z", value: 89.07 },
        { date: "2020-03-31T00:00:00.000Z", value: 73.48 },
        { date: "2020-04-27T00:00:00.000Z", value: 79.63 },
        { date: "2020-05-19T00:00:00.000Z", value: 76.25 },
        { date: "2020-06-10T00:00:00.000Z", value: 86.62 },
        { date: "2020-07-01T00:00:00.000Z", value: 89.2 },
        { date: "2020-07-22T00:00:00.000Z", value: 94.68 },
        { date: "2020-08-12T00:00:00.000Z", value: 96.43 },
        { date: "2020-09-02T00:00:00.000Z", value: 98.14 },
        { date: "2020-09-23T00:00:00.000Z", value: 94.61 },
        { date: "2020-10-15T00:00:00.000Z", value: 98.96 },
        { date: "2020-11-05T00:00:00.000Z", value: 102.83 },
        { date: "2020-11-27T00:00:00.000Z", value: 110.08 },
        { date: "2020-12-21T00:00:00.000Z", value: 113.04 },
        { date: "2021-01-12T00:00:00.000Z", value: 123.76 },
        { date: "2021-02-03T00:00:00.000Z", value: 125.01 },
        { date: "2021-02-24T00:00:00.000Z", value: 127.04 },
        { date: "2021-03-18T00:00:00.000Z", value: 123.4 },
        { date: "2021-04-12T00:00:00.000Z", value: 121.61 },
        { date: "2021-05-05T00:00:00.000Z", value: 124.75 },
        { date: "2021-05-27T00:00:00.000Z", value: 130.99 },
        { date: "2021-06-17T00:00:00.000Z", value: 134.12 },
        { date: "2021-07-08T00:00:00.000Z", value: 134.5 },
        { date: "2021-07-30T00:00:00.000Z", value: 134.99 },
        { date: "2021-08-23T00:00:00.000Z", value: 140.32 },
        { date: "2021-09-14T00:00:00.000Z", value: 148.78 },
        { date: "2021-10-05T00:00:00.000Z", value: 152.19 },
        { date: "2021-10-27T00:00:00.000Z", value: 154.77 },
        { date: "2021-11-22T00:00:00.000Z", value: 149.04 },
        { date: "2021-12-13T00:00:00.000Z", value: 148.67 },
        { date: "2022-01-03T00:00:00.000Z", value: 150.43 },
        { date: "2022-01-24T00:00:00.000Z", value: 146.05 },
        { date: "2022-02-15T00:00:00.000Z", value: 147.79 },
        { date: "2022-03-09T00:00:00.000Z", value: 139.47 },
        { date: "2022-03-31T00:00:00.000Z", value: 148.63 },
        { date: "2022-04-25T00:00:00.000Z", value: 145.6 },
        { date: "2022-05-17T00:00:00.000Z", value: 138.85 },
        { date: "2022-06-07T00:00:00.000Z", value: 139.29 },
        { date: "2022-06-28T00:00:00.000Z", value: 134.73 },
        { date: "2022-07-19T00:00:00.000Z", value: 139.82 },
        { date: "2022-08-10T00:00:00.000Z", value: 149.68 },
        { date: "2022-09-02T00:00:00.000Z", value: 150.78 },
        { date: "2022-09-23T00:00:00.000Z", value: 149.12 },
        { date: "2022-10-17T00:00:00.000Z", value: 147.79 },
        { date: "2022-11-10T00:00:00.000Z", value: 153.74 },
        { date: "2022-12-01T00:00:00.000Z", value: 159.68 },
        { date: "2022-12-22T00:00:00.000Z", value: 153.83 },
        { date: "2023-01-12T00:00:00.000Z", value: 151.73 },
        { date: "2023-02-03T00:00:00.000Z", value: 148.86 },
      ],
    },
    durations: ["1m", "6m", "1y", "3y", "max"],
    defaultDuration: "max",
  },
};
