import React, { Component } from "react";
import { Card, CardBody, Stack, Heading, Text} from "@chakra-ui/react";
import { FaBlog, FaDownload, FaFlask } from "react-icons/fa";

export default class Blogs extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <div>
        <Card
          direction={{ base: "column", sm: "row" }}
          overflow="hidden"
          variant="outline"
        >
          <FaBlog size={50} style={{ margin: "20px", color: "#2d7be0" }} />
          <Stack>
            <CardBody>
              <Heading size="md" color="#2d7be0">
                Block Post
              </Heading>
              <Text py="2">Read more about equity & Debt</Text>
            </CardBody>
          </Stack>
        </Card>
        <Card
          direction={{ base: "column", sm: "row" }}
          overflow="hidden"
          variant="outline"
        >
          <FaFlask size={50} style={{ margin: "20px", color: "#2d7be0" }} />
          <Stack>
            <CardBody>
              <Heading size="md" color="#2d7be0">
                Methodology
              </Heading>
              <Text py="2">Know How this smallCase was created</Text>
            </CardBody>
          </Stack>
        </Card>
        <Card
          direction={{ base: "column", sm: "row" }}
          overflow="hidden"
          variant="outline"
        >
          <FaDownload size={50} style={{ margin: "20px", color: "#2d7be0" }} />
          <Stack>
            <CardBody>
              <Heading size="md" color="#2d7be0">
                Fact Sheet
              </Heading>
              <Text py="2">Download key points of this smallcase</Text>
            </CardBody>
          </Stack>
        </Card>
      </div>
    );
  }
}
