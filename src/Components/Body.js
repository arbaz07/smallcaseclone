import React, { Component } from "react";
import { getSmallCase } from "../Api/SmallCaseApi";
import BodyTitle from "../Components/Body-Title/BodyTitle";
import SideBar from "../Components/Sidebar/SideBar";
import AboutManager from "../Components/MainBody/AboutManager";
import AboutSM from "../Components/MainBody/AboutSM";
import Blogs from "../Components/MainBody/Blogs";
import { Flex } from "@chakra-ui/react";
import Graph from "./MainBody/Chart/Graph";
import ChartSelector from './MainBody/Chart/ChartSelector'
import Chart from "./MainBody/Chart/Chart";
// import NavBar from "./MainBody/NavBar";
// import { connect } from 'react-redux';
// import { setData } from '../redux/action/Actions';
// import { dispatch } from 'redux';

export default class Body extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: "",
      scid: "SCET_0013",
    };
  }

  componentDidMount = async () => {
    try {
      const smallCaseData = await getSmallCase(this.state.scid);
      this.setState({
        data: smallCaseData,
      });
    } catch (error) {
      // console.log("error :>> ", error);
    }
  };

  render() {
    // console.log("this.state.data :>> ", this.state.data);
    if (this.state.data == "") {
      return <></>;
    }
    return (
      <div>
        <BodyTitle
          info={this.state.data.data.info}
          stats={this.state.data.data.stats.ratios}
        />
        {/* <NavBar/> */}
        <Flex justify="space-around" margin='20' >
          <Flex direction='column'>
            <Flex justify="space-around" style={{margin:'5'}}>
              <AboutSM rational={this.state.data.data.rationale} />
              <Blogs />
            </Flex>
            <Chart/>
          </Flex>

          <SideBar stats={this.state.data.data.stats} />
        </Flex>
        <AboutManager />
      </div>
    );
  }
}

// function mapStateToProps(state){
//   return {
//     state
//   }
// }

// function mapMapDispatchToProps(dispatch){
//   return{
//     setData:(data)=>{dispatch(setData(data))},

//   }
// }
// export default connect(mapStateToProps,mapMapDispatchToProps)(Body);
