import React, { Component } from "react";
import {
  Card,
  Image,
  Stack,
  CardBody,
  Box,
  Heading,
  Button,
  Text,
  Flex,
} from "@chakra-ui/react";

export default class BodyTitle extends Component {

  render() {
    // console.log('this.props.info :>> ', this.props.info);
    const {publisherName,name,shortDescription} =this.props.info;
    const {cagrDuration,cagr} = this.props.stats

    return (
      <Flex justify='space-between' backgroundColor='#f9fafb' margin='5'>
        <Box marginLeft='20' backgroundColor='#f9fafb' fontSize={20}>
          <Card
            direction={{ base: "column", sm: "row" }}
            overflow="hidden"
            variant="outline"
            backgroundColor='#f9fafb'
          >
            <Image
              objectFit="cover"
              maxW={{ base: "100%", sm: "200px" }}
              src="https://assets.smallcase.com/images/smallcases/160/SCET_0013.png"
              alt="img"
            />
            <Stack>
              <CardBody backgroundColor='#f9fafb'>
                <Box display="flex">
                  <Heading size="lg">{name}</Heading>
                  <Button colorScheme="blue" size="xs" margin='auto'>
                    free Access
                  </Button>
                </Box>

                <Text py="1">Managed by {publisherName}</Text>
                <Text py="4">
                  {shortDescription}
                </Text>
              </CardBody>
            </Stack>
          </Card>
        </Box>
        <Box margin='auto' paddingLeft='40'>
          <Stack direction="row" spacing={4}>
            <Stack>
              <Text>{cagrDuration} CGAR</Text>
              <Text fontSize='25'color='#19af55'>{(parseFloat(cagr)*100).toFixed(2)}%</Text>
            </Stack>
            <Button colorScheme="teal" variant="solid">
              Lo Volatility
            </Button>
          </Stack>
        </Box>
      </Flex>
    );
  }
}
