import { Flex } from "@chakra-ui/react";
import React, { Component } from "react";
import {
  Card,
  Heading,
  CardBody,
  Text,
  Button,
  Stack,
  Divider,
} from "@chakra-ui/react";
import { InfoIcon } from "@chakra-ui/icons";
// import AiFillFacebook

import {
  FaRupeeSign,
  FaFacebook,
  FaTwitter,
  FaWhatsapp,
  FaGithub,
  FaCopy,
} from "react-icons/fa";

export default class SideBar extends Component {
  render() {
    const {minInvestAmount}=this.props.stats;
    return (
      <div>
        <Card maxW="sm">
          <CardBody>
            <Stack mt="6" spacing="3">
              <Flex>
                <Heading size="md">Minimum Investment Amount</Heading>
                <InfoIcon margin="auto" boxSize="5" />
              </Flex>
              <Flex justifyItems="center">
                <FaRupeeSign fontSize="2xl" />
                <Text fontSize="2xl">{minInvestAmount}</Text>
              </Flex>
            </Stack>
            <Stack mt="6" spacing="3">
              <Text fontSize="2xl">Get free access forever</Text>
              <Text fontSize="2xl" color="blue">
                {" "}
                See more benefits
              </Text>
            </Stack>
          </CardBody>

          <Stack>
            <Button
              margin="4"
              color="white"
              padding="7"
              backgroundColor="#27bc94"
              _hover={{ bg: "#2EDEAF" }}
            >
              Invest Now
            </Button>
            <Button margin="4" padding="7">
              Add To Watchlist
            </Button>
          </Stack>

          <Divider />

          <Flex justifyContent='space-around' marginTop="10" marginBottom='10'>
              <Text>Share On</Text>
              <FaFacebook size="25"/>
              <FaTwitter size="25" />
              <FaWhatsapp size="25" />
              <FaGithub size="25" />
              <FaCopy size="25" />
          </Flex>
        </Card>
      </div>
    );
  }
}
