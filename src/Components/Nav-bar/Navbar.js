import {
  Box,
  Button,
  ButtonGroup,
  Container,
  Flex,
  HStack,
  IconButton,
  useBreakpointValue,
  Image,
  Divider,
} from '@chakra-ui/react'
import { FaBell } from 'react-icons/fa';

const Navbar = () => {
  return (
    <Box as="section" pb={{ base: '12', md: '24' }}>
          <HStack spacing="10" justify="space-around">
              <Flex justify="space-around" flex="1">
                <ButtonGroup variant="link" spacing="8">
                  <Image boxSize='100' margin='0' src='https://www.smallcase.com/static/svgs/logo-full.svg'></Image>
                  {['Home','Discover','Create'].map((item) => (
                    <Button key={item}>{item}</Button>
                  ))}
                </ButtonGroup>
                <ButtonGroup variant="link" spacing="8">
                  {['Watch List','Investment',<FaBell/>,'Account'].map((item) => (
                    <Button key={item}>{item}</Button>
                  ))}
                </ButtonGroup>
              </Flex>
          </HStack>
              <Divider/>
    </Box>
  )
}

export default Navbar;
